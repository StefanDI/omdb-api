import React from 'react';
import {Route, Router, BrowserRouter} from 'react-router-dom'
import {createBrowserHistory} from 'history';

import SearchPage from "./components/search-page/SearchPage";
import SingleMoviePage from "./components/single-movie-page/SingleMoviePage";
import './reset-css.css';
import './OmdbApp.css';

function OmdbApp() {
    return (
        <BrowserRouter history={createBrowserHistory()} basename={process.env.PUBLIC_URL}>
            <Route exact path={"/"} component={SearchPage}/>
            {/*<Route path={"/:query"} component={SearchPage}/>*/}
            <Route path={"/movie/:id"} component={SingleMoviePage}/>
        </BrowserRouter>
    );
}

export default OmdbApp;


